<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Delivery calculation</title>

    <!-- Bootstrap core CSS -->
    <link href="static/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <style type="text/css">
        .form-control {
            max-width: 250px;
            display: inline-block!important;
        }
        .row {
            width: 100%!important;
        }
        form label {
            width: 120px;
        }
    </style>
    <script src="static/jquery.min.js"></script>
