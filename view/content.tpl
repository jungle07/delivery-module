<body>
<div class="container mt-5">
    <form id="delivery-from" action="" method="POST">
        <input type="hidden" name="calc" value="1">
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label>Сервис доставки</label>
                    <select name="service" class="form-control">
                        <option value="">Все</option>
                        <?php foreach($services as $service) { ?>
                        <option value="<?= $service['key'] ?>"><?= $service['name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="shipperAddress">От куда</label>
                    <select class="form-control" id="shipperAddress">
                        <option value="0">Выбрать регион</option>
                    </select>
                    <select name="from" class="form-control mr-2" id="shipperAddress1">
                        <option value="0">Выбрать город</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label for="receiverAddress">Куда</label>
                    <select class="form-control" id="receiverAddress">
                        <option value="0">Выбрать регион</option>
                    </select>
                    <select name="to" class="form-control mr-2" id="receiverAddress1">
                        <option value="0">Выбрать город</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="form-group">
                    <label>Вес</label>
                    <input type="text" class="form-control numeric" name="weight" value="0">
                </div>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" class="button btn btn-secondary">Рассчитать</button>
        </div>
    </form>
    <?php if (!empty($response)) { ?>
        <?php if ($type == 'single') { ?>
            <?php if (empty($response['error'])) { ?>
                <div class="alert alert-success" role="alert">
                    <ul class="list-group">
                        <li class="list-group-item">Сервис доставки: <?= $services[$_POST['service']]['name'] ?></li>
                        <li class="list-group-item">Откуда: <?= $_POST['from'] ?> >> Куда: <?= $_POST['to'] ?></li>
                        <li class="list-group-item">Цена: <?= $response['price'] ?> руб</li>
                        <li class="list-group-item">Дата доставки: <?= $response['date'] ?></li>
                    </ul>
                </div>
                <?php } else { ?>
                <div class="alert alert-danger" role="alert">
                    <?= $response['error'] ?>
                </div>
            <?php } ?>
        <?php } else { 
            foreach ($response as $key => $item) { ?>

                <?php if (empty($item['error'])) { ?>
                    <div class="alert alert-success" role="alert">
                        <ul class="list-group">
                            <li class="list-group-item">Сервис доставки: <?= $services[$key]['name'] ?></li>
                            <li class="list-group-item">Откуда: <?= $_POST['from'] ?> >> Куда: <?= $_POST['to'] ?></li>
                            <li class="list-group-item">Цена: <?= $item['price'] ?> руб</li>
                            <li class="list-group-item">Дата доставки: <?= $item['date'] ?></li>
                        </ul>
                    </div>
                    <?php } else { ?>
                    <div class="alert alert-danger" role="alert">
                        <?= $response['error'] ?>
                    </div>
                <?php } ?>

            <?php } ?>
        <?php } ?>
    <?php } ?>
</div>
</body>
<script type="text/javascript">
    var locations = <?= json_encode($list) ?>;
    $(document).ready(function(){
        for (var i = 0; i < locations.length; i++) {
            if (locations[i]['parent_id'] == 0) {
                $('#shipperAddress').append('<option value="'+ locations[i]['ID'] +'">'+ locations[i]['name'] +'</option>');
                $('#receiverAddress').append('<option value="'+ locations[i]['ID'] +'">'+ locations[i]['name'] +'</option>');
            }
        }
        $('#shipperAddress').change(function() {
            loadLocations($(this).val(), 'shipperAddress1');
        });
        $('#receiverAddress').change(function() {
            loadLocations($(this).val(), 'receiverAddress1');
        });
        $('#delivery-from').submit(function() {
            if ($('#shipperAddress option:selected').val() == 0 || $('#receiverAddress option:selected').val() == 0) {
                alert('Выберите регион и город');
                return false;
            }
            if ($('#shipperAddress1 option:selected').val() != '0' 
                && $('#receiverAddress1 option:selected').val() != '0' 
                && $('#shipperAddress1 option:selected').val() == $('#receiverAddress1 option:selected').val()
            ) {
                alert('Пункт отправки и назачения не могут быть один и тем же адресом');
                return false;
            }
        });
    });

    var loadLocations = function(parentID, el) {
        if (!parentID) {
            return;
        }
        $('#' + el).empty();
        for (var i = 0; i < locations.length; i++) {
            if (locations[i]['parent_id'] == parentID) {
                $('#' + el).append('<option value="'+ locations[i]['name'] +'">'+ locations[i]['name'] +'</option>');
            }
        }
    }

    var controlFields = function() {
        if ($('#shipperAddress option:selected').val() > 0 && $('#receiverAddress option:selected').val() > 0) {
            $('.button').prop('disabled', false);
        } else {
            $('.button').prop('disabled', true);
        }
    }
</script>
</head>
</html>
