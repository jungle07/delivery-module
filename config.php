<?php
date_default_timezone_set('Europe/Moscow');

$config = [
    'dbhost' => 'localhost',
    'dbuser' => '',
    'dbpassword' => '',
    'database' => '',
    'dbport' => '3306',
];

