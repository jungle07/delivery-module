<?php

namespace Delivery;

use Delivery\DBManager;

require_once 'config.php';
require_once 'vendor/autoload.php';

$db = new DBManager();

$from = $db->escapeValue(urldecode($_REQUEST['from']));
$to = $db->escapeValue(urldecode($_REQUEST['to']));

if (!$from || !$to) {
    echo 0;
    return;
}

$sql = "SELECT `ID`, `lat`, `lng` FROM `locations` WHERE `name` = '{$from}' ";
$shipperAddress = $db->getRow($sql);

$sql = "SELECT `ID`, `lat`, `lng` FROM `locations` WHERE `name` = '{$to}' ";
$receiverAddress = $db->getRow($sql);

if (!$shipperAddress || !$receiverAddress) {
    echo 0;
    return;
}

$latFrom = (float) $shipperAddress['lat'];
$lngFrom = (float) $shipperAddress['lng'];
$latTo = (float) $receiverAddress['lat'];
$lngTo = (float) $receiverAddress['lng'];

$theta = $lngFrom - $lngTo; 
$distance = (sin(deg2rad($latFrom)) * sin(deg2rad($latTo))) + (cos(deg2rad($latFrom)) * cos(deg2rad($latTo)) * cos(deg2rad($theta))); 
$distance = acos($distance); 
$distance = rad2deg($distance); 
$distance = $distance * 60 * 1.1515; 

// convert to km
$distance = $distance * 1.609344; 
$distance = round($distance,2);

echo (float) $distance;
return;
