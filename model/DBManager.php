<?php

namespace Delivery;

class DBManager
{
    /*
    * @var object of mysqli class;
    */
    public $mysqli;

    /*
     * constructor
     */
    public function __construct()
    {
        $this->connect();
    }

    public function connect()
    {
        global $config;

        mysqli_report(MYSQLI_REPORT_STRICT);
        try {
            $this->mysqli = new \mysqli($config['dbhost'], $config['dbuser'], $config['dbpassword'], $config['database'], $config['dbport']);
            
            if (!$this->mysqli->connect_error) {
                $this->mysqli->set_charset('utf8');
            } else {
                $this->printError(null, $this->mysqli->connect_error);
            }   
        } catch(mysqli_sql_exception $e) {
            die('<span style="color: brown;">Failed to connect to MySQL database</span>');
        }
    }

    public function query($sql = false)
    {
        if (!$sql) {
            return false;
        }

        $result = $this->mysqli->query($sql);
        if (!$result) {
            $this->printError($sql);
        }

        return $result; 
    }

    public function get($sql = false)
    {
        if(!$sql)
        {
            return false;
        }

        $result = $this->query($sql);
        
        if($result)
        {
            return $this->adaptResultQueryToAssociatedData($result);
        }
    }

    public function getRow($sql = false)
    {
        if(!$sql)
        {
            return false;
        }

        $result = $this->query($sql);
        
        if($result)
        {
            return $this->adaptResultQueryToAssociatedData($result, true);
        }
    }

    protected function printError($sql = '', $error = false)
    {
        $trace = debug_backtrace();
        $level = 0;
        $class_name = get_class($this);

        foreach($trace as $index => $trValue)
        {
            if($trValue['class'] == $class_name)
            {
                $level = $index;
            }
        }

        clDebugger::write(($error ? $error : $this->mysqli->error)."on Line #{$trace[$level]['line']} (File: {$trace[$level]['file']}, method: {$trace[$level]['function']})", $trace[$level]['type']);

        /* display message */
        if(CL_DB_ERRORS)
        {
            $message = '<p>
                            <strong>File: '.$trace[$level]['file'].'</strong><br />
                            <strong>Method: '.$trace[$level]['function'].'</strong><br />
                            <strong>Line: '.$trace[$level]['line'].'</strong><br /><br />
                            <strong>MySQL error: <span style="color: brown;">'.($error ? $error : $this->mysqli->error).'</span></strong><br />
                            <span>MySQL query: '.$sql.'</span>
                        </p>';
            die($message);
        }
    }
    
    /**
    * Protect from sql injection
    * 
    * @param $data - single value or array;
    */
    public function escapeValue($data)
    {
        if (is_array($data)) {
            foreach ($data as $key => $val) {
                $data[$key] = $this->escapeValue($val);
            }
        } else {
            $data = $this->mysqli->real_escape_string($data);
        }
        
        return $data;
    }

    protected function adaptResultQueryToAssociatedData(&$result, $single = false)
    { 
        $response = array();

        while ($row = $result->fetch_assoc()) {
            if ($single) {
                $response = $row;
                break;
            } else {
                $response[] = $row;
            }
        }

        $result->free();

        return $response;
    }
}
