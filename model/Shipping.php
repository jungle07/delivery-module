<?php

namespace Delivery;

use Delivery\DBManager;

class Shipping
{
    /**
     * Get single quote by service
     *
     * @param string $service
     * @param string $shipperAddress
     * @param string $receiverAddress
     * @param double $weight
     * @return array
     */
    public function getSingleQuote($service = '', $shipperAddress = '', $receiverAddress = '', $weight = 0)
    {
        if (!$service || !$shipperAddress || !$receiverAddress) {
            return [];
        }

        $methodClass = '\Delivery\\' . ucfirst($service);
        $instance = new $methodClass();

        $instance->setOption('shipperAddress', $shipperAddress);
        $instance->setOption('receiverAddress', $receiverAddress);
        $instance->setOption('weight', $weight);

        $quote = json_decode($instance->getQuote(), true);
        $quote['date'] = is_int($quote['period']) 
        ? date("Y-m-d", strtotime("+{$quote['period']} days", time())) 
        : $quote['period'];

        return json_encode($quote);
    }

    /**
     * Get multi quote by services
     *
     * @param string $shipperAddress
     * @param string $receiverAddress
     * @param double $weight
     * @return array
     */
    public function getMultiQuote($shipperAddress = '', $receiverAddress = '', $weight = 0)
    {
        if (!$shipperAddress || !$receiverAddress) {
            return [];
        }

        $db = new DBManager();

        $sql = "SELECT * FROM `services`";
        $services = $db->get($sql);

        if (!$services) {
            return [];
        }

        foreach ($services as $key => $value) {
            $methodClass = '\Delivery\\' . ucfirst($value['key']);
            $instance = new $methodClass();

            $instance->setOption('shipperAddress', $shipperAddress);
            $instance->setOption('receiverAddress', $receiverAddress);
            $instance->setOption('weight', $weight);

            $quote = json_decode($instance->getQuote(), true);

            $quote['date'] = is_int($quote['period']) 
            ? date("Y-m-d", strtotime("+{$quote['period']} days", time())) 
            : $quote['period'];
            $result[$value['key']] = $quote;
        }

        return json_encode($result);
    }
}
