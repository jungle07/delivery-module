<?php

namespace Delivery;

abstract class Service
{
    /**
     * Request data
     *
     * @var array
     */
    protected $request;

    /**
     * Method info
     *
     * @var array
     */
    protected $methodInfo;

    /**
     * Calculate shipping cost
     */
    public function getQuote()
    {}

    /**
     * Set shipping option
     *
     * @param string $key
     * @param string $value
     */
    public function setOption($key = '', $value = '')
    {
        if (empty($key)) {
            return;
        }

        $this->request[$key] = $value;
    }

    /**
     * Get source from value
     *
     * @param string $key
     *
     * @return string
     */
    public function getOption($key = '')
    {
        if (empty($key)) {
            return null;
        }

        return $this->request[$key];
    }

    /**
     * Get Method
     *
     * @return array
     */
    public function getMethod()
    {
        return $this->methodInfo;
    }
}
