<?php

namespace Delivery;

use Delivery\Service;

class Fast extends Service
{
    /**
     * API URL
     *
     * @var string
     */
    protected $apiURL = '/request.php';

    /**
     * Price for 1 km
     *
     * @var double
     */
    protected $priceForKm = 5;

    /**
     * Price for 1 kg
     *
     * @var double
     */
    protected $priceForKg = 30;

    /**
     * Period (1 day = 100 km)
     *
     * @var int
     */
    protected $period = 100;

    /**
     * Distance
     *
     * @var double
     */
    protected $distance = 0;

    /**
     * Calculate shipping cost
     *
     * @return json
     */
    public function getQuote()
    {
        // input data
        $shipperAddress = $this->getOption('shipperAddress');
        $receiverAddress = $this->getOption('receiverAddress');

        $error = '';
        $apiURL = $this->getAPIURL();
        $apiURL .= '?from=' . urlencode($shipperAddress) . '&to=' . urlencode($receiverAddress);

        try {
            $distance = file_get_contents($apiURL);

            if ($distance > 0) {
                $this->setDistance($distance);
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
        }

        $out = [
            'price' => $this->calculate(),
            'period' => $this->getPeriod(),
            'error' => $error
        ];

        return json_encode($out);
    }

    /**
     * Get API URL
     *
     * @return string
     */
    public function getAPIURL()
    {
        $uri = explode('/', $_SERVER['REQUEST_URI']);
        array_pop($uri);
        $uri = implode('/', $uri);

        $host = "{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}" . rtrim($uri);

        return $host . $this->apiURL;
    }

    /**
     * Calculate price
     *
     * @return double
     */
    public function calculate()
    {
        $distance = $this->getDistance();
        $weight = $this->getOption('weight');
        $weightPrice = $weight * $this->priceForKg;
        $distancePrice = $distance * $this->priceForKm;

        $price = $weightPrice + $distancePrice;

        return round($price, 2);
    }

    /**
     * Calculate price
     * 
     * @return int
     */
    public function getPeriod()
    {
        date_default_timezone_set('Europe/Moscow');

        $now = new \DateTime();
        $currentHour = $now->format('H');

        if ($currentHour < 18) {
            $currentTime = time();
        } else {
            $hours = 15;
            $modified = (clone $now)->add(new \DateInterval("PT{$hours}H"));
            $currentTime = $modified->getTimestamp();
        }

        $oneDay = 60 * 60 * 24;
        $distance = $this->getDistance();
        $days = $distance < 100 ? 1 : round($distance / $this->period);

        $endTime = $currentTime + ($oneDay * $days);
        $diffDays = round(($endTime - time()) / $oneDay);

        return $diffDays;
    }

    /**
     * Set distance
     *
     * @param string $value
     */
    public function setDistance($value = '')
    {
        $this->distance = (float) $value;
    }

    /**
     * Get distance
     *
     * @return double
     */
    public function getDistance()
    {
        if (empty($this->distance)) {
            return 0;
        }

        return $this->distance;
    }
}
