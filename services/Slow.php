<?php

namespace Delivery;

use Delivery\Service;

class Slow extends Service
{
    /**
     * API URL
     *
     * @var string
     */
    protected $apiURL = '/request.php';

    /**
     * Basic price
     *
     * @var double
     */
    protected $basicPrice = 150;

    /**
     * Coefficient for 1 km
     *
     * @var double
     */
    protected $coefficient = 0.03;

    /**
     * Price for 1 kg
     *
     * @var double
     */
    protected $priceForKg = 20;

    /**
     * Period (1 day = 50 km)
     *
     * @var int
     */
    protected $period = 50;

    /**
     * Distance
     *
     * @var double
     */
    protected $distance;

    /**
     * Calculate shipping cost
     *
     * @return json
     */
    public function getQuote()
    {
        // input data
        $shipperAddress = $this->getOption('shipperAddress');
        $receiverAddress = $this->getOption('receiverAddress');

        $error = '';
        $apiURL = $this->getAPIURL();
        $apiURL .= '?from=' . urlencode($shipperAddress) . '&to=' . urlencode($receiverAddress);

        try {
            $distance = file_get_contents($apiURL);

            if ($distance > 0) {
                $this->setDistance($distance);
            }
        } catch (Exception $e) {
            $error = $e->getMessage();
        }

        $out = [
            'price' => $this->calculate(),
            'period' => $this->getPeriod(),
            'error' => $error
        ];

        return json_encode($out);
    }

    /**
     * Get API URL
     *
     * @return string
     */
    public function getAPIURL()
    {
        $uri = explode('/', $_SERVER['REQUEST_URI']);
        array_pop($uri);
        $uri = implode('/', $uri);

        $host = "{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}" . rtrim($uri);

        return $host . $this->apiURL;
    }

    /**
     * Calculate price
     *
     * @return double
     */
    public function calculate()
    {
        $distance = $this->getDistance();
        $weight = $this->getOption('weight');
        $weightPrice = $weight * $this->priceForKg;
        $distancePrice = $distance * $this->coefficient;

        $price = ($this->basicPrice * $distancePrice) + $weightPrice;

        return round($price, 2);
    }

    /**
     * Calculate price
     * 
     * @return int
     */
    public function getPeriod()
    {
        $distance = $this->getDistance();
        $days = $distance < 50 ? 1 : round($distance / $this->period);

        return date("Y-m-d", strtotime("+{$days} days", time()));
    }

    /**
     * Set distance
     *
     * @param string $value
     */
    public function setDistance($value = '')
    {
        $this->distance = (float) $value;
    }

    /**
     * Get distance
     *
     * @return double
     */
    public function getDistance()
    {
        if (empty($this->distance)) {
            return 0;
        }

        return $this->distance;
    }
}
