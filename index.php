<?php

use Delivery\Shipping;
use Delivery\DBManager;

require_once 'config.php';
require_once 'vendor/autoload.php';

$db = new \Delivery\DBManager();

$sql = "SELECT `ID`, `parent_id`, `name`, `lat`, `lng` FROM `locations` order by `name` asc ";
$list = $db->get($sql);

$sql = "SELECT `ID`, `key`, `name` FROM `services` order by `name` asc ";
$servicesTmp = $db->get($sql);

$services = [];
foreach ($servicesTmp as $key => $value) {
    $services[$value['key']] = $value;
}

if (isset($_POST['calc'])) {
    $shipping = new Shipping();

    $type = !empty($_POST['service']) ? 'single' : 'multi';
    $service = $_POST['service'];
    $shipperAddress = $_POST['from'];
    $receiverAddress = $_POST['to'];
    $weight = $_POST['weight'];

    if ($type == 'single') {
        $response = $shipping->getSingleQuote($service, $shipperAddress, $receiverAddress, $weight);
    }
    else {
        $response = $shipping->getMultiQuote($shipperAddress, $receiverAddress, $weight);
    }

    $response = json_decode($response, true);
}

include 'view/header.tpl';
include 'view/content.tpl';
